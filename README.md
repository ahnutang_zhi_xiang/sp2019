### 介绍
#### 连享会2019空间计量专题课程主页。
#### 分享课程相关的资料和常见问题解答。

### 空间计量课程相关资料
#### 1.空间计量相关软件
链 接：https://pan.baidu.com/s/1cLusaL2DnzUnTuO6cQyyTA 
提取码：q7ZR  
#### 2.空间计量相关课件
链 接：https://pan.baidu.com/s/11oCH-UMHkthxqs37hKwxEw
提取码：2wji
#### 3.学员上课座次表
上课地址：西北工业大学国际会议中心二楼第五会议室

![image.png](https://images.gitee.com/uploads/images/2019/0626/170238_d1d71d98_4954883.png)

#### 4.课程时间安排
2019年6 月27日—30 日，共计3天； 每天安排：9:00—12:00；14:00—17:00,17：00—17:30 答疑。
### 软件安装常见问题解答
#### 1.stata更新不了？
解答：更新包没有下载完全或者没有下载，下载后通过更新包手动进行更新均能完成更新。更新的时候要看教程，不要进行联网更新，群里提供的软件不是正版授权的软件，是破解的，联网更新肯定会失败。
#### 2.执行程序报错，错误提示见下图：

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_ad8a13c3_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_3ac020ba_4954883.png)

解答：程序写错，在进行关联的时候，尽量不要手打程序，能复制的就复制，不一样的地方再手动修改，刚刚几个同学的问题基本都是程序输入的问题，重点注意程序中的 空格 引号 与下横线，千万不要漏写，一旦漏写就会报错。

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_8e093e27_4954883.png)

除了图中空格漏写外，还有cd后面漏写空格的，cd后面路径只有一半引号的，stata_kernel.install中间下横线漏写的，请务必仔细检查程序！！！
#### 3.matlab破解不了，出现下面对话框？

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_b2c9ed66_4954883.png)

解答：原因可能有两个，一个是把matlab装到了系统盘（C盘），matlab是收费软件，群里提供的是破解版，装在系统盘就会报错；此外还有可能没有进行破解，有一部分同学没有下载授权文件，按照安装教程进行破解（主要是复制粘贴两个文件）。
#### 4.Jupyter notebook打不开怎么办？
解答：Jupyter notebook打开方式有很多，一种不行就换一种。下面提供一些打开Jupyter notebook的方法。
#### 方案1：

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_5012c5f4_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_7b5f3a00_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_d300a919_4954883.png)

#### 方案2：

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_c52934e7_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_d129fd1a_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_6b5b749b_4954883.png)

#### 方案3：

![image.png](https://images.gitee.com/uploads/images/2019/0625/212518_0d717114_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_1aaaa20f_4954883.png)

#### 方案4：

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_51fbf4ee_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_128d4512_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212520_a8271343_4954883.png)

#### 5.Jupyter notebook打开后没办法上传课件，或者上传后没反应找不到课件？
解答：浏览器不兼容，傲游浏览器会出错，谷歌没问题，其他浏览器待测试。
#### 6.关联matla或者stata时，程序正确但出现报错，例如下面的错误提示。

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_1ba37ae3_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_249c81ec_4954883.png)

解答：没有以管理员身份打开软件，以管理员身份打开时错误就消失了。
#### 7.matlab安装调用慢的问题？
解答：耐心等待，matlab程序包18.8G，安装后占用空间29.7G，软件大，安装自然比较慢，调用也慢，耐心等一下，如果长时间（超过30分钟）无任何变化可关闭程序重新操作。
#### 8.什么情况下才算是关联成功？
解答：出现下图中的界面则安装成功。

![image.png](https://images.gitee.com/uploads/images/2019/0625/212519_24729dc5_4954883.png)

![image.png](https://images.gitee.com/uploads/images/2019/0625/212520_a44f8cf0_4954883.png)

#### 9.安装软件要出要联网？
解答：三个软件的安装不需要联网，但是stata与python关联以及matlab与python关联下载程序包时需要联网，关联成功后每次使用不需要联网，不需要联网的意思是连不连都行，对操作没影响。
#### 10.mac软件关联的问题？
解答：目前尚未进行测试，具体方法未知，群里有mac系统的小伙伴可以寻找方法后发到群里。
#### 11.关联过程中程序什么时候才算执行结束？
解答：出现刚弹出窗口自动显示的那一行指令就可以了；打开powershell或者prompt界面的时候窗口自动有一行指令，程序执行完也会有这行命令，没有出现则在执行中。

![image.png](https://images.gitee.com/uploads/images/2019/0625/212520_9081457b_4954883.png)

#### 12.弹出如下界面如何操作？

![image.png](https://images.gitee.com/uploads/images/2019/0625/212520_1b3aa3fc_4954883.png)

解答：所有y/n选择，都在后面输入y，然后打回车，有的电脑会遇到该问题，有的不会遇到。